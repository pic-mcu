                    PIC Example Projects Using Mikropascal

PIC Microcontrollers: A Revolting Architecture

   The [1]Microchip [2]PIC microcontroller family has a revolting
   architecture, with only a single accumulator register, register bank
   switching, and an inefficient stack implementation. My impressions were
   formed years ago in assembly language days, but it is still a revolting
   architecture for C programs, too.

   I also find the [3]Microchip development software to be expensive,
   bloated, and hard to use.

   Despite all of the complaints above, there is something weirdly
   compelling about a microcontroller like the [4]PIC16F1455 that comes in
   a 14-pin [5]DIP package, has a USB device interface, requires no
   external components except one bypass capacitor, and sells for a little
   over $1 USD.

   Microchip continues to sell many interesting PIC microcontrollers in
   DIP packages. Such devices are easy to prototype with using solderless
   breadboards, if you can stand to write the software for them. They are
   also very easy to buy in small quantities from [6]Microchip Direct.

mikroPascal for PIC

   The [7]Mikroelektronika [8]mikroPascal for PIC Pascal compiler makes
   working with PIC devices bearable. The compiler hides almost all of the
   ugliness of the PIC architecture.

   Furthermore, mikroPascal has a very extensive [9]software component
   library for the PIC, ranging from trigonometry functions to USB HID
   device support. I am convinced that the true value of a microcontroller
   development environment lies in the richness of its software component
   libraries more than any other factor. In this regard, mikroPascal for
   PIC excels and is worth every penny of its $249 USD price (which
   includes lifetime updates).
   _______________________________________________________________________

   Questions or comments to Philip Munts [10]phil@munts.net

   I am available for custom system development (hardware and software) of
   products using PIC or other (please!) microcontrollers.

References

   1. http://www.microchip.com/
   2. https://en.wikipedia.org/wiki/PIC_microcontroller
   3. http://www.microchip.com/mplab/mplab-x-ide
   4. http://www.microchip.com/wwwproducts/en/PIC16F1455
   5. https://en.wikipedia.org/wiki/Dual_in-line_package
   6. https://www.microchipdirect.com/
   7. http://www.mikroe.com/
   8. http://www.mikroe.com/mikropascal/pic
   9. http://www.mikroe.com/mikropascal/pic/libraries
  10. mailto:phil@munts.net
