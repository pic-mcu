{ Simple HID device program for the PIC16F1455 }

{ Copyright (C)2017-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_usbhid;

VAR
  LED  : sbit AT LATA5_bit;
  cmd  : ARRAY [HID_OUTPUT_REPORT_BYTES] OF Char; LDM; ABSOLUTE $0A0;
  resp : ARRAY [HID_INPUT_REPORT_BYTES] OF Char; LDM; ABSOLUTE $120;

  PROCEDURE interrupt();

  BEGIN
    USB_Interrupt_Proc();
  END;

BEGIN
  OSCCON := $FC; { Enable 3x PLL }
  ANSELA := $00; { PORTA all GPIO }
  ANSELC := $00; { PORTC all GPIO }

  { Configure the LED }
  TRISA.5 := 0;
  LED := False;

  { Initialize HID library }
  HID_Enable(@cmd, @resp);

  { Main event loop }
  WHILE True DO
    BEGIN
      { Check for message from host }
      IF (HID_Read() <> 0) THEN
        BEGIN
          IF strncmp(cmd, 'LEDON', 5) = 0 THEN
            LED := True;

          IF strncmp(cmd, 'LEDOFF', 6) = 0 THEN
            LED := False;
        END;
    END;
END.
